import React, { Component } from 'react';
import './App.css';
import './components/WordInput/WordInput';
import WordInput from "./components/WordInput/WordInput";
import LettersBlock from "./components/LettersBlock/LettersBlock";
import Start from "./components/StartGame/StartGame"
import ImageBlock from "./components/ImageBlock/ImageBlock"
class App extends Component {

  constructor(props){
      super(props);
      this.state = {
        word : "",
        start : false,
        secondPlayer : false,
        lifeRest: 6,
        gameWin: false,
      }
  }

  submitText = (text) =>{
          this.setState({
              word: text,
              secondPlayer: true,
          });

    };

    handlerStart = () =>{
        this.setState({
            start: true
        });
    }

    handleLifeReduction = () => {
        this.setState({
            lifeRest: this.state.lifeRest - 1
        })
    }

    handlerRestart = () =>{
        this.setState({
            word : "",
            start : true,
            secondPlayer : false,
            lifeRest: 6,
            gameWin: false,
        })
    }

    handlerExit = () =>{
        this.setState({
            word : "",
            start : false,
            secondPlayer : false,
            lifeRest: 6,
            gameWin: false,
        })
    }

    handlerGameWin =() =>{
        this.setState({
            gameWin: true
        })
    }

  render() {
    return (
      <div className="App">
        <header className="App-header">
           Шибениця
        </header>
          <ImageBlock lifeRest = {this.state.lifeRest}/>
          {!this.state.start &&
            <Start handlerStart = {this.handlerStart}/>}
          {this.state.start &&
            !this.state.secondPlayer &&
            <WordInput submitText = {this.submitText} />}
          {Boolean(this.state.lifeRest) &&
            this.state.secondPlayer &&
              <LettersBlock
                text = {this.state.word}
                handlerGameWin = {this.handlerGameWin}
                handleLifeReduction={this.handleLifeReduction}/>}
          {!this.state.lifeRest &&
            <div className="gameOver">Ви програли. Спробуйте ще!</div>}
          {this.state.gameWin &&
                <div className="gameWin">Ви виграли!</div>}
          {(!this.state.lifeRest || this.state.gameWin) && <button
              className="restart"
              onClick={this.handlerRestart}>Нова гра</button>}
          {this.state.start &&
          <button
              className="exit-btn"
              onClick={this.handlerExit}>Вихід</button>}
      </div>
    );
  }
}

export default App;
