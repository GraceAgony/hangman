import React from 'react';
import * as Constants from  '../../constants/constants';
import PropTypes from "prop-types";

const Alphabet = (props) =>{
    let alphabet= Constants.ALPHABET.trim();
    let children =[];
    for(let i=0; i<alphabet.length; i++){
        children.push(
            <button className="letter-btn"
                    key={i}
                    onClick={(event)=>handlerOnClick(event, props)}>
                {alphabet[i].toUpperCase()}
            </button>
        );
    }
    return <div className="alphabet">{children}</div>;
}

const handlerOnClick = (event, props) => {
    if(event.target.disabled === false) {
        event.target.disabled = true;
        event.target.classList.add('disabled');
    }
    props.handlerClickLetter(event.target.innerText);
}

Alphabet.propTypes ={
    handlerClickLetter: PropTypes.func.isRequired
}

export default Alphabet;