import React from 'react';
import PropTypes from "prop-types";

const LettersPlaces = (props) => {
    let text = props.text.split(' ');
    return <div className="letters-places">{Words(text)}</div>
}

const Words = (text) =>{
    let wordsFirstLetterIndex = 0;
    return text.map((word, i) => {
            var letters = word.split("").map((letter, letterIndex) =>
                <div
                    className="hidden-letter"
                    key={wordsFirstLetterIndex + letterIndex}
                    data-number={wordsFirstLetterIndex + letterIndex}>

                </div>
            )
            wordsFirstLetterIndex += word.length;
            return (<div className="word-container">
                        <div className="word" key={i}>{letters}</div>
                        <div className="space" key={i}></div>
                    </div>)
        }
    )
}

LettersPlaces.propTypes ={
    text: PropTypes.string.isRequired
}

export default LettersPlaces;