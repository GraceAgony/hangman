import React, { Component } from 'react';
import PropTypes from "prop-types";
import Alphabet from "./Alphabet";
import LettersPlaces from "./LettersPlaces";
import './LettersBlock.css';

class LettersBlock extends Component{

    constructor(props){
        super(props);
        let text = this.props.text.split(" ").join("").toUpperCase()
        this.state = {
            text: text,
            lettersToGuess: text.length
        }
    }


    handlerClickLetter=(letter)=>{
            let letterIndex = this.state.text.indexOf(letter);
            let lettersToGuess= this.state.lettersToGuess;
            if (letterIndex === -1) {
                this.props.handleLifeReduction()
            } else {
                while (letterIndex !== -1 ) {
                    let elem = document.querySelector(`[data-number="${letterIndex}"`);
                    elem.innerText = letter;
                    letterIndex = this.state.text.indexOf(letter, ++letterIndex);
                    lettersToGuess--;
                    this.setState({
                        lettersToGuess: lettersToGuess
                })
                    if(lettersToGuess === 0){
                        this.props.handlerGameWin();
                    }
                }
            }
    }

    render(){
        return(
            <div className="letters-block">
                <LettersPlaces text={this.props.text}/>
                {Boolean(this.state.lettersToGuess) &&
                <Alphabet handlerClickLetter={this.handlerClickLetter}/>}
            </div>
        )
    }
}




LettersBlock.propTypes = {
    text: PropTypes.string.isRequired,
    handlerGameWin: PropTypes.func.isRequired,
    handleLifeReduction: PropTypes.func.isRequired
};

export default LettersBlock;