import React, { Component } from 'react';
import * as Constants from  '../../constants/constants'
import "./WordInput.css";
import PropTypes from "prop-types";

class WordInput extends Component{
    constructor(props){
        super(props);
        this.state = {
            inputText: "",
            emptyWord: false,
        }
    }

    submitText = (event) =>{
        event.preventDefault();
        let words = this.state.inputText.split(' ').map((word) =>
            word = word.trim()
        ).join(' ').trim();
        if(words.length === 0){
            this.setState({
                emptyWord: true
            })
        }else {
            this.setState({inputText: words});
            this.props.submitText(words);
        }
    };

    handleChange = (event) => {

        this.setState({inputText : this.validate(event.target.value)})
    };

    validate(text){
        for(let i=0; i<text.length; i++) {
            if (Constants.ALPHABET.indexOf(text[i].toLowerCase()) === -1) {
                return text.slice(0, i);
            }
        }
        return text;
    }


    render(){
        return(
            <div className="word-input">
                <div>Загадайте слово</div>
                {this.state.emptyWord &&
                    <div>Будь ласка, загадайте не пусте слово</div>
                }
                <form className="word-form" onSubmit={this.submitText}>
                    <label>
                        <input className="text-input" type="text" value={this.state.inputText} onChange={this.handleChange}/>
                    </label>
                    <button type="submit" className="submit-btn">Загадати</button>
                </form>
                <span>
                    можна вводити лише літери кирилиці (А-Я) та пробіл
                </span>
            </div>
        )

    }
}

WordInput.propTypes = {
    submitText : PropTypes.func.isRequired
}

export default WordInput