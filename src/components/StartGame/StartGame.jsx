import React from 'react';
import PropTypes from "prop-types";

const Start = props => {
    return(
        <button
            onClick={props.handlerStart}
            className="start-btn"
            id="start-btn">
            Старт</button>
    )
}

Start.propTypes = {
    handlerStart: PropTypes.func.isRequired
}

export default Start;