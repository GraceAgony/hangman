import React, {Component} from 'react';
import PropTypes from "prop-types";
import "./ImageBlock.css";

class ImageBlock extends Component{

    constructor(props){
        super(props);
        this.state = {
            image: "img1"
        }
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.lifeRest !== prevState.lifeRest){
            let imgNumber = 7- nextProps.lifeRest;
            return {image: 'img'+ imgNumber}
        }
        else return null;
    }

    render(){
        return(
            <div className="img-container">
                <img alt={`${this.state.image}`}
                     className="img"
                     src={require(`./../../../images/${this.state.image}.png`)}>

                </img>
            </div>
        )
    }
}

ImageBlock.propTypes = {
    lifeRest: PropTypes.number.isRequired,
};

export default ImageBlock;